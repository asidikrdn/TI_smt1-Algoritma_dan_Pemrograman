/*Ahmad Sidik Rudini*/
/*201106041165*/
/*Angka Mutu*/
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
main()
{
	int i, N, AM[50], NPM[50], NK[50], NQ[50], NT[50], NUTS[50], NUAS[50];
	char Nama[50][100],Mutu[50],MK[100];
	
	printf("Silahkan Masukkan Nama Mata Kuliah : ");
	scanf("%s",&MK);
	printf("Berapa jumlah mahasiswa yang ingin dilihat nilai mutunya ?\n=>");
	scanf("%d",&N);
	for(i=0; i<N ;i++) 
	{
		printf("\n\nSilahkan Masukkan Nama Mahasiswa : ");
		scanf("%s",&Nama[i]);
		printf("Silahkan Masukkan NPM Mahasiswa : ");
		scanf("%d",&NPM[i]);
		printf("Silahkan Masukkan Nilai Kehadiran Mahasiswa : ");
		scanf("%d",&NK[i]);
		printf("Silahkan Masukkan Nilai Quis Mahasiswa : ");
		scanf("%d",&NQ[i]);
		printf("Silahkan Masukkan Nilai Tugas Mahasiswa : ");
		scanf("%d",&NT[i]);
		printf("Silahkan Masukkan Nilai UTS Mahasiswa : ");
		scanf("%d",&NUTS[i]);
		printf("Silahkan Masukkan Nilai UAS Mahasiswa : ");
		scanf("%d",&NUAS[i]);
		
		AM[i]=(NK[i]*30/100)+(NQ[i]*5/100)+(NT[i]*15/100)+(NUTS[i]*20/100)+(NUAS[i]*30/100);
		
		if(AM[i]>=80)
		{
			Mutu[i]='A';
		}
	
		else if(AM[i]>=70)
		{
			Mutu[i]='B';
		}
	
		else if(AM[i]>=60)
		{
			Mutu[i]='C';
		}
	
		else if(AM[i]>=50)
		{
			Mutu[i]='D';
		}
	
		else
		{
			Mutu[i]='E';
		}
		
	}
	
	printf ("=================================================================== \n");
	printf("Mata Kuliah = %s",MK);
	printf ("\n NPM \t Nama \t Kehadiran \t Quis \t Tugas \t UTS \t UAS \t Mutu \n");
	printf ("=================================================================== \n");
	
	for(i=0; i<N ;i++)
	{
		printf ("\n%d \t %s \t %d \t\t %d \t %d \t %d \t %d \t %c", NPM[i], Nama[i], NK[i], NQ[i], NT[i], NUTS[i], NUAS[i], Mutu[i]);
	}
	return 0;
}

