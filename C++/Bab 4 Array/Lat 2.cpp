/*Ahmad Sidik Rudini*/
/*201106041165*/
/*PT.Paket Kilat*/
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
main()
{
	int i, N, K[i], D[50], Bp[50], Ba, Bpk, Np[50], Op[50];
	char Tp[50][100];
	Ba = 100;		//Biaya Administrasi
	Bpk = 150;		//Biaya Per KG
	
	printf("Berapa banyak paket yang akan dibuat laporannya ? \n=>");
	scanf("%d", &N);
	
	for(i=0;i<N;i++)
	{
		printf("\nMasukkan Nomor Paket\n=>");
		scanf("%d",&Np[i]);
		printf("Masukkan Berat Paket\n=>");
		scanf("%d",&Bp[i]);
		printf("Masukkan Tujuan Paket\n=>");
		scanf("%s",&Tp[i]);
		
		if(Bp[i]<=2){
			D[i]=0;
			K[i]=Ba+(Bpk*2);
			Op[i]=K[i]+D[i];
		}
		
		else if(Bp[i]<=20){
			D[i]=0;
			K[i]=Ba+(Bpk*Bp[i]);
			Op[i]=K[i]+D[i];
		}
		
		else if(Bp[i]>20){
			D[i]=Bp[i]*50;
			K[i]=Ba+(Bpk*Bp[i]);
			Op[i]=K[i]+D[i];
		}
		
		else if(Bp[i]>40){
			D[i]=Bp[i]*100;
			K[i]=Ba+(Bpk*Bp[i]);
			Op[i]=K[i]+D[i];
		}
	}
	
	printf("============================================================================\n");
	printf("\n\nNo \t Resi \t Berat \t Dst \t Admin \t Ongkir \t Tmbhn \t Total \n");
	printf("============================================================================\n");
	
	for(i=0;i<N;i++)
	{
		printf("\n%d \t %d \t %d \t %s \t  %d \t %d \t\t %d \t %d", i+1, Np[i], Bp[i], Tp[i], Ba, (Bp[i]*Bpk), D[i], Op[i]);
	}
	printf("\n============================================================================\n");
}
