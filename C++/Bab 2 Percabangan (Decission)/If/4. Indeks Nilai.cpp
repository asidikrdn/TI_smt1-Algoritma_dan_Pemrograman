// Ahmad Sidik Rudini - 201106041165
// Menentukan Indeks Nilai Mahasiswa
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>

main()
{
	int Nilai;
	
	nanya:											// Label
		
	printf("Silahkan Masukkan Nilai Anda : ");
	scanf("%d",&Nilai);
	
	if(Nilai>=80)
	{
		printf("Indeks Nilai Anda adalah A \n\n");
	}
	
	else if(Nilai>=70)
	{
		printf("Indeks Nilai Anda adalah B \n\n");
	}
	
	else if(Nilai>=60)
	{
		printf("Indeks Nilai Anda adalah C \n\n");
	}
	
	else if(Nilai>=50)
	{
		printf("Indeks Nilai Anda adalah D \n\n");
	}
	
	else
	{
		printf("Indeks Nilai Anda adalah E \n\n");
	}
	
	goto nanya;											// Kembali menuju label
}
