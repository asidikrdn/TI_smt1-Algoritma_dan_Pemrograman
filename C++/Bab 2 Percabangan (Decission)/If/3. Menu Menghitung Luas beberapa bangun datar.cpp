// Ahmad Sidik Rudini - 201106041165
// Memilih Menu Perhitungan Luas Lingkaran, Segitiga, Bujur Sangkar

#include <stdio.h>
#include <conio.h>
#include <stdlib.h>

main()
{
	int Menu;
	
	nanya :
	printf("Silahkan pilih menu berikut : \n1. Menghitung Luas Lingkaran \n2. Menghitung Luas Segitiga \n3. Menghitung Luas Bujur Sangkar \n0. Exit \n\nMenu yang anda pilih => ");
	scanf("%d",&Menu);
	
	if(Menu==1)		// Menghitung Luas Lingkaran
	{
		float Ll,rl;
		float phi=3.14;
	
		printf("\nMasukkan jari-jarinya :");
		scanf("%f",&rl);
	
		Ll=phi*rl*rl;
	
		printf("Luasnya adalah : %f\n \n\n\n",Ll);
		goto nanya;
	}
	
	else if(Menu==2)		// Menghitung Luas Segitiga
	{
		int as,ts,Ls;
		printf("\nMasukkan alas : ");
		scanf("%d",&as);
		printf("Masukkan tinggi : ");
		scanf("%d",&ts);
	
		Ls=as*ts/2;		// alas*tinggi/ 2
	
		printf("Luas segitiganya adalah : %d \n\n\n",Ls);
		goto nanya;
	}	
	
	else if(Menu==3)		// Menghitung Luas Bujur Sangkar
	{
		int sb,Lb;
		printf("\nMasukkan sisi bujur sangkar : ");
		scanf("%d",&sb);
	
		Lb=sb*sb;		// sisi x sisi
	
		printf("Luas bujur sangkarnya adalah : %d \n\n\n",Lb);
		goto nanya;
	}	
	
	else if(Menu==0)		// Exit
	{
		exit(0);
	}
	
	else
	{
		printf("\nMenu yang anda pilih salah ! \n\n\n");
		goto nanya;
	}
}
