// Ahmad Sidik Rudini - 201106041165
// Menentukan Upah Pekerja Berdasarkan Golongan

#include <stdio.h>
#include <conio.h>
#include <stdlib.h>

main()
{
	int ul, JJK,Gaji;
	char gol;
	
	ul = 3000;
	
	printf("Silahkan masukkan golongan karyawan : (A, B, C ,D) => ");
	scanf("%c",&gol);

	printf("Silahkan masukkan jumlah jam kerja karyawan : ");
	scanf("%d",&JJK);													// Pertanyaan, kok kalo jumlah jam diinput lebih dulu sebelum golongan hasilnya jadi 0 ?
	
	if(gol=='A'||gol=='a')
	{
		if(JJK<=48)
		{
			Gaji=JJK*4000;
		}
		else
		{
			Gaji = (48*4000)+((JJK-48)*ul);
		}
		printf("Upah karyawan tersebut adalah : Rp %d \n\n", Gaji);
	}
	
	else if(gol=='B'||gol=='b')
	{
		if(JJK<=48)
		{
			Gaji=JJK*5000;
		}
		else
		{
			Gaji = (48*5000)+((JJK-48)*ul);
		}
		printf("Upah karyawan tersebut adalah : Rp %d \n\n", Gaji);
	}
	
	else if(gol=='C'||gol=='c')
	{
		if(JJK<=48)
		{
			Gaji=JJK*6000;
		}
		else
		{
			Gaji = (48*6000)+((JJK-48)*ul);
		}
		printf("Upah karyawan tersebut adalah : Rp %d \n\n", Gaji);
	}
	
	else if(gol=='D'||gol=='d')
	{
		if(JJK<=48)
		{
			Gaji=JJK*7000;
		}
		else
		{
			Gaji = (48*7000)+((JJK-48)*ul);
		}
		printf("Upah karyawan tersebut adalah : Rp %d \n\n", Gaji);
	}
	
	else
	{
		printf("Golongan yang anda pilih tidak ada ! \n\n");
	}
	getch();
	
}
