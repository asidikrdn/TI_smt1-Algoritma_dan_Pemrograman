// Ahmad Sidik Rudini - 201106041165
// Menentukan Huruf Vokal atau Konsonan

#include <stdio.h>
#include <conio.h>

main()
{
	char H;

	printf("Silahkan masukkan huruf yang diinginkan : ");
	scanf("%c",&H);
	
	if(H=='A'||H=='a'||H=='I'||H=='i'||H=='U'||H=='u'||H=='E'||H=='e'||H=='O'||H=='o')
	{
		printf("Huruf yang anda masukkan (%c) adalah huruf vokal \n\n",H);
	}
	
	else
	{
		printf("Huruf yang anda masukkan (%c) adalah huruf konsonan \n\n",H);
	}
	
	getch();
}
