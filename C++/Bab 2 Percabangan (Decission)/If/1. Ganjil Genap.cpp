// Ahmad Sidik Rudini - 201106041165
// Menentukan Bilangan Ganjil dan Genap

#include <stdio.h>
#include <conio.h>

main()
{
	int Bil;
	
	nanya:
		
	printf("Silahkan masukkan bilangan yang diinginkan : ");
	scanf("%d",&Bil);
	
	if(Bil%2==0)							// Kondisi Pertama, jika memenuhi syarat untuk bilangan genap, maka akan tampil teks bilangan genap
	{
		printf("Bilangan Genap \n\n");
		goto nanya;
	}
	
	else									// Kondisi Kedua, jika tak memenuhi syarat untuk bilangan genap, maka akan tampil teks bilangan ganjil
	{
		printf("Bilangan Ganjil \n\n");
		goto nanya;
	}
	
	getch();
}
