// Ahmad Sidik Rudini - 201106041165
// Menentukan Tahun Kabisat

#include <stdio.h>
#include <conio.h>
#include <stdlib.h>

main()
{
	int Tahun;
	
	nanya:
	
	printf("Silahkan masukkan tahun yang diinginkan : ");
	scanf("%d",&Tahun);
	
	if(Tahun%4==0)							// Kondisi Pertama, jika memenuhi syarat untuk tahun kabisat
	{
		printf("Tahun %d adalah Tahun Kabisat \n\n",Tahun);
		
		goto nanya;
	}
	
	else									// Kondisi Kedua, jika tak memenuhi syarat untuk tahun kabisat
	{
		printf("Tahun %d bukanlah Tahun Kabisat \n\n",Tahun);
		
		goto nanya;
	}
	
	getch();
}
