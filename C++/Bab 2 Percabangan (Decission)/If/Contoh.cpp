// Ahmad Sidik Rudini - 201106041165
// Menentukan Lulus dan Tidak Lulus

#include <stdio.h>
#include <conio.h>

main()
{
	int Nilai;
	
	printf("Silahkan masukkan nilai siswa : ");
	scanf("%d",&Nilai);
	
	if(Nilai>=60)							// Kondisi Pertama
	{
		printf("Siswa dinyatakan Lulus");
	}
	
	else									// Kondisi Kedua, jika tak memenuhi syarat untuk lulus
	{
		printf("Siswa dinyatakan Tidak Lulus");
	}
	
	getch();
}
