// Ahmad Sidik Rudini - 201106041165
// Menentukan Wujud Air

#include <stdio.h>
#include <conio.h>

main()
{
	int air;
	
	nanya:
	
	printf("Silahkan masukkan suhu air : ");
	scanf("%d",&air);
	
	if(air<=0)													// Kondisi Pertama, jika memenuhi syarat untuk wujud padat
	{
		printf("Wujudnya Padat \n\n");
		goto nanya;
	}
	
	else if(air>0 && air<=100)									// Kondisi Kedua, jika tak memenuhi syarat untuk wujud padat namun memenuhi syarat wujud cair
	{
		printf("Wujudnya cair \n\n");
		goto nanya;
	}
	
	else														// Kondisi Ketiga, jika tak memenuhi syarat untuk wujud padat atau cair
	{
		printf("Wujudnya gas \n\n");
		goto nanya;
	}
	
	getch();
}
