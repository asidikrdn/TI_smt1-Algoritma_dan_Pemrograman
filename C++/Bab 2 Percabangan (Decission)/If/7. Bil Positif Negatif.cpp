// Ahmad Sidik Rudini - 201106041165
// Menentukan Bilangan Positif dan Negatif

#include <stdio.h>
#include <conio.h>

main()
{
	int Bil;
	
	nanya:
	
	printf("Silahkan masukkan bilangan yang diinginkan : ");
	scanf("%d",&Bil);
	
	if(Bil>0)							// Kondisi Pertama, jika memenuhi syarat untuk bilangan positif
	{
		printf("Bilangan Positif \n\n");
		goto nanya;
	}
	
	else if(Bil<0)									// Kondisi Kedua, jika tak memenuhi syarat untuk bilangan positif namun memenuhi syarat untuk bilangan negatif
	{
		printf("Bilangan Negatif \n\n");
		goto nanya;
	}
	
	else									// Kondisi Ketiga, jika tak memenuhi syarat untuk bilangan positif atau negatif
	{
		printf("Bilangan Nol \n\n");
		goto nanya;
	}
	
	getch();
}
