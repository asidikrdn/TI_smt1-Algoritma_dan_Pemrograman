// Ahmad Sidik Rudini - 201106041165
// Menentukan Upah Pekerja
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>

main()
{
	int gol, gp, ul, JJK, Gaji;
	int i, N;
	
	ul = 3000;

	printf("Berapa jumlah karyawan yang ingin dilihat jumlah gajinya ?\n=>");
	scanf("%d",&N);
	for(i=1; i<=N ;i++) {
		
		gp=0;
		
		printf("Daftar Golongan : \n1. Golongan A \n2. Golongan B \n3. Golongan C \n4. Golongan D \n");
		printf("Silahkan pilih golongan karyawan dengan menginputkan nomor golongannya\n=>");
		scanf("%d",&gol);
				
		switch(gol)
		{
			case 1 :
				gp=4000;
				break;
			case 2 :
				gp=5000;
				break;
			case 3 :
				gp=6000;
				break;
			case 4 :
				gp=7000;
				break;		
			default :
				printf("Nomor yang anda inputkan salah! Silahkan Coba lagi");
				break;			
		}

		printf("Silahkan masukkan jumlah jam kerja karyawan %d : ", i);
		scanf("%d",&JJK);
		
		if(JJK<=48)
		{
			Gaji=JJK*gp;
		}
		else
		{
			Gaji = (48*gp)+((JJK-48)*ul);
		}
		printf("Upah karyawan tersebut adalah : Rp %d \n\n", Gaji);
	}
	return 0;
	
	
}
