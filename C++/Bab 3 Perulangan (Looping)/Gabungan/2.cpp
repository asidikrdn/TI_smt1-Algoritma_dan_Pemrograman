/*Ahmad Sidik Rudini*/
/*201106041165*/
/*Angka Mutu*/
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
main()
{
	int i, N, AM, NK, NQ, NT, NUTS, NUAS;
	char Nama[100],Mutu;
	
	printf("Berapa jumlah mahasiswa yang ingin dilihat nilai mutunya ?");
	scanf("%d",&N);
	for(i=1; i<=N ;i++) 
	{
		printf("Silahkan Masukkan Nama Mahasiswa : ");
		scanf("%s",&Nama);
		printf("Silahkan Masukkan Nilai Kehadiran Mahasiswa : ");
		scanf("%d",&NK);
		printf("Silahkan Masukkan Nilai Quis Mahasiswa : ");
		scanf("%d",&NQ);
		printf("Silahkan Masukkan Nilai Tugas Mahasiswa : ");
		scanf("%d",&NT);
		printf("Silahkan Masukkan Nilai UTS Mahasiswa : ");
		scanf("%d",&NUTS);
		printf("Silahkan Masukkan Nilai UAS Mahasiswa : ");
		scanf("%d",&NUAS);
		
		AM=(NK*30/100)+(NQ*5/100)+(NT*15/100)+(NUTS*20/100)+(NUAS*30/100);
		
		if(AM>=80)
		{
			Mutu='A';
		}
	
		else if(AM>=70)
		{
			Mutu='B';
		}
	
		else if(AM>=60)
		{
			Mutu='C';
		}
	
		else if(AM>=50)
		{
			Mutu='D';
		}
	
		else
		{
			Mutu='E';
		}
		
		printf("\nNilai Mutu Mahasiswa bernama %s adalah %c \n\n\n",Nama, Mutu );
	}
	return 0;
	
}

