/*Ahmad Sidik Rudini*/
/*201106041165*/
/*Perulangan While*/
#include <stdio.h>
int main(){
	int i=1, batas;
	printf("Angka 1 s/d berapa yang ingin anda lihat ?");
	scanf("%d", &batas);
	while(i<=batas){
		printf("%d \n",i);
		i++;
	}
	return 0;
}
