/*Ahmad Sidik Rudini*/
/*201106041165*/
/*Perulangan While*/
/*Bilangan Genap < 100*/
#include <stdio.h>
int main(){
	int i=2;
	while(i<100){
		printf("%d \n", i);
		i+=2;
	} 
	return 0;
}
