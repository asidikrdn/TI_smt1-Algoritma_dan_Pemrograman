/*Ahmad Sidik Rudini*/
/*201106041165*/
/*Perulangan Do-While*/
#include <stdio.h>
int main(){
	int i=1;
	do{
		printf("%d \n",i);
		i++;
	} while(i<=20);
	return 0;
}
