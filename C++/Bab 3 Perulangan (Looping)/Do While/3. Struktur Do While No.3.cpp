/*Ahmad Sidik Rudini*/
/*201106041165*/
/*Perulangan Do-While*/
#include <stdio.h>
int main(){
	int i=1, batas;
	printf("Angka 1 s/d berapa yang ingin anda lihat ?");
	scanf("%d", &batas);
	do{
		printf("%d \n",i);
		i++;
	} while(i<=batas);
	return 0;
}
