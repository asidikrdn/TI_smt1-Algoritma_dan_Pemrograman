/*Ahmad Sidik Rudini*/
/*201106041165*/
/*Perulangan Do-While*/
#include <stdio.h>
main()
{
	int i=1, batas, jumlah=1;
	
	printf("Berapa banyak deret yang akan dijumlahkan ? ");
	scanf("%d", &batas);
	printf("\nJumlah dari deret \n");
	printf("%d", i);
	
	i=2;
	
	do{
		printf(" + %d",i);
		jumlah=jumlah+i;
		i++;
	} 
	while(i<=batas);
	
	printf("\nadalah : ");
	printf("%d", jumlah);	
	
	return 0;
}
