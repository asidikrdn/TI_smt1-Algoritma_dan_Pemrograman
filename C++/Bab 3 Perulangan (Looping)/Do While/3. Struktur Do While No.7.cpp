/*Ahmad Sidik Rudini*/
/*201106041165*/
/*Perulangan Do-While*/
/*Bilangan Ganjil < 100*/
#include <stdio.h>
int main(){
	int i=1;
	do{
		printf("%d \n", i);
		i+=2;
	}
	while(i<100);
	return 0;
}
