/*Ahmad Sidik Rudini*/
/*201106041165*/
/*Perulangan Do-While*/
#include <stdio.h>
main()
{
	int i=1, batas;
	float jumlah, rata, bil;
	
	printf("Berapa banyak bilangan dihitung rata-ratanya ? ");
	scanf("%d", &batas);
	
	do{
		printf("Silahkan Masukkan Bilangan ke-%d : ", i);
		scanf("%f", &bil);
		jumlah=jumlah+bil;
		rata=jumlah/i;
		i++;
	} 
	while(i<=batas);
	
	printf("\nJumlahnya adalah : ");
	printf("%f", jumlah);
	printf("\nRata-ratanya adalah : ");
	printf("%f", rata);
	
	return 0;
}
