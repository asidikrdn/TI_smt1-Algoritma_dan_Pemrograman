//Nama : Ahmad Sidik Rudini (201106041165)
//Latihan 1 Materi Struktur 

#include <stdio.h>
#include <conio.h>

struct
{
	int npm;
	char nama[30];
	int na;
	char am;
}
mhs[20];	

main()
{
	int i,b;
	
	printf("Berapa jumlah mahasiswa yang akan dihitung ? ");
	scanf("%d", &b);
	
	for(i=0;i<b;i++)
	{
		printf("Masukkan NPM : ");
		scanf("%d",&mhs[i].npm);
		printf("Masukkan Nama : ");
		scanf("%s",&mhs[i].nama);
		printf("Masukkan Nilai Angka: ");
		scanf("%d",&mhs[i].na);
		
		if(mhs[i].na>=80){
			mhs[i].am='A';
		}
		else if(mhs[i].na>=70){
			mhs[i].am = 'B';
		}
		else if(mhs[i].na>=60){
			mhs[i].am = 'C';
		}
		else if(mhs[i].na>=50){
			mhs[i].am = 'D';
		}
		else if(mhs[i].na<=49){
			mhs[i].am = 'E';
		}
	}
	
	printf("-------------------------------------------------");
	printf("\nNo. \t NPM \t Nama \t Nilai Angka \t Nilai Huruf");
	printf("\n-------------------------------------------------");
	for(i=0;i<b;i++)
	{
		printf("\n%d \t %d \t %s \t %d \t\t %c",i+1,mhs[i].npm,mhs[i].nama,mhs[i].na,mhs[i].am);
	}
	printf("\n-------------------------------------------------");
	
	getch();
}
