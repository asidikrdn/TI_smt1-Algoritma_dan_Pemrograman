//Nama : Ahmad Sidik Rudini (201106041165)
//Latihan 2 Materi Struktur 

#include <stdio.h>
#include <conio.h>

struct
{
	int np;
	int bp;
	char tp[20];
	int op;
	int bt;
}
pkt[20];	

main()
{
	int i,b,ba,bpk;
	ba=100;
	bpk=150;
	
	printf("Berapa jumlah paket yang akan dihitung ? ");
	scanf("%d", &b);
	
	for(i=0;i<b;i++)
	{
		printf("Masukkan Nomor Paket : ");
		scanf("%d",&pkt[i].np);
		printf("Masukkan Berat Paket : ");
		scanf("%d",&pkt[i].bp);
		printf("Masukkan Tujuan Paket : ");
		scanf("%s",&pkt[i].tp);
		
		if(pkt[i].bp<=2){
			pkt[i].bt=0;
			pkt[i].op=ba+(2*bpk);
		}
		else if(pkt[i].bp<=20){
			pkt[i].bt=0;
			pkt[i].op=ba+(pkt[i].bp*bpk);
		}
		else if(pkt[i].bp<=40){
			pkt[i].bt=50;
			pkt[i].op=ba+(pkt[i].bp*bpk)+(pkt[i].bt*pkt[i].bp);
		}
		else if(pkt[i].bp>40){
			pkt[i].bt=100;
			pkt[i].op=ba+(pkt[i].bp*bpk)+(pkt[i].bt*pkt[i].bp);
		}
	}
	printf("---------------------------------------------------------------------------------");
	printf("\nNo. \t Nomor \t Berat \t Dst \t Admin \t Ongkir \t Tmbhn \t Total");
	printf("\n---------------------------------------------------------------------------------");
	for(i=0;i<b;i++)
	{
		printf("\n%d \t %d \t %d \t %s \t %d \t %d \t\t %d \t %d",i+1,pkt[i].np,pkt[i].bp,pkt[i].tp,ba,(pkt[i].bp*bpk),(pkt[i].bt*pkt[i].bp),pkt[i].op);
	}
	printf("\n---------------------------------------------------------------------------------");
	getch();
}
