//Nama : Ahmad Sidik Rudini (201106041165)
//Latihan 3 Materi Struktur 

#include <stdio.h>
#include <conio.h>

struct
{
	int npm;
	char nama[30];
	char matkul[30];
	int nk,nq,nt,nuts,nuas;
	int na;
	char am;
}
mhs[20];	

main()
{
	int i, b;
	
	printf("Berapa jumlah mahasiswa yang akan dihitung ? ");
	scanf("%d", &b);
	
	for(i=0;i<b;i++)
	{
		printf("Masukkan NPM : ");
		scanf("%d",&mhs[i].npm);
		printf("Masukkan Nama : ");
		scanf("%s",&mhs[i].nama);
		printf("Masukkan Matakuliah : ");
		scanf("%s",&mhs[i].matkul);
		printf("Masukkan Nilai Kehadiran : ");
		scanf("%d",&mhs[i].nk);
		printf("Masukkan Nilai Quis : ");
		scanf("%d",&mhs[i].nq);
		printf("Masukkan Nilai Tugas : ");
		scanf("%d",&mhs[i].nt);
		printf("Masukkan Nilai UTS : ");
		scanf("%d",&mhs[i].nuts);
		printf("Masukkan Nilai UAS : ");
		scanf("%d",&mhs[i].nuas);
		
		mhs[i].na = (mhs[i].nk*30/100)+(mhs[i].nq*5/100)+(mhs[i].nt*15/100)+(mhs[i].nuts*20/100)+(mhs[i].nuas*30/100);
		
		if(mhs[i].na>=80){
			mhs[i].am='A';
		}
		else if(mhs[i].na>=70){
			mhs[i].am = 'B';
		}
		else if(mhs[i].na>=60){
			mhs[i].am = 'C';
		}
		else if(mhs[i].na>=50){
			mhs[i].am = 'D';
		}
		else if(mhs[i].na<=49){
			mhs[i].am = 'E';
		}
	}
	
	printf("\n\n----------------------------------------------------------------------------");
	printf ("\n NPM \t Nama \t Matkul\t Absen \t Quis \t Tugas \t UTS \t UAS \t Mutu \n");
	printf("\n----------------------------------------------------------------------------");
	for(i=0;i<b;i++)
	{
		printf ("\n %d \t %s \t %s \t %d \t %d \t %d \t %d \t %d \t %c \n",mhs[i].npm,mhs[i].nama,mhs[i].matkul,mhs[i].nk,mhs[i].nq,mhs[i].nt,mhs[i].nuts,mhs[i].nuas,mhs[i].am );
	}
	getch();
}
