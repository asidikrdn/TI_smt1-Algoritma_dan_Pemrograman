// Ahmad Sidik Rudini - 201106041165
// Menghitung Konversi Suhu dari Fahreinreit ke Celcius dan Reamur

#include <stdio.h>
#include <conio.h>

main()
{
	int r,f,c;
	
	printf("Silahkan masukkan suhu dalam derajat Fahrenheit :");
	scanf("%d",&f);
	
	c=(f-32)*5/9;
	r=(f-32)*4/9;
	
	printf("Suhu dalam derajat Celciusnya adalah : %d\n",c);
	printf("Suhu dalam derajat Reamurnya adalah : %d\n",r);
	
	getch();
	
}
