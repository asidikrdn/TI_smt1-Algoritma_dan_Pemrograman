// Ahmad Sidik Rudini - 201106041165
// Menghitung Luas dan Sisi Miring Segitiga Siku-siku

#include <stdio.h>
#include <conio.h>
#include <math.h>

main()
{
	float L,A,B,C;
	
	printf("Masukkan sisi siku-siku pertama : ");
	scanf("%f",&A);
	
	printf("Masukkan sisi siku-siku kedua : ");
	scanf("%f",&B);
	
	
	L=A*B/2;
	C=sqrt((pow(A,2))+(pow(B,2)));			// sqrt = operasi mencari akar kuadrat, pow = operasi perpangkatan (angka,jumlah pangkatnya)
	
	printf("Luasnya adalah : %f\n",L);
	printf("Sisi miringnya adalah : %f\n", C);
	
	getch();
}
