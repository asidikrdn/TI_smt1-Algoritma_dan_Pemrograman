// Ahmad Sidik Rudini - 201106041165
// Menghitung Luas dan Volume Bola

#include <stdio.h>
#include <conio.h>
#include <math.h>

main()
{
	float L,I,R;    //L=Luas permukaan bola, I=Volume bola, R=Jari-jari bola
	float phi=3.14;
	
	printf("Masukkan jari-jari bola : ");
	scanf("%f",&R);
		
	L=(pow(R,2))*phi*4;
	I=((pow(R,3))*phi*4)/3;
	
	printf("Luas permukaannya adalah : %f\n",L);
	printf("Volumenya adalah : %f\n", I);
	
	getch();
}
