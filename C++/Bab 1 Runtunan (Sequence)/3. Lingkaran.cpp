// Ahmad Sidik Rudini - 201106041165
// Menghitung Luas dan Keliling Lingkaran

#include <stdio.h>
#include <conio.h>

main()
{
	float L,K,r;
	float phi=3.14;
	
	printf("Masukkan jari-jarinya :");
	scanf("%f",&r);
	
	L=phi*r*r;
	K=phi*r*2;
	
	printf("Luasnya adalah : %f\n",L);
	printf("Kelilingnya adalah : %f\n", K);
	
	getch();
}
