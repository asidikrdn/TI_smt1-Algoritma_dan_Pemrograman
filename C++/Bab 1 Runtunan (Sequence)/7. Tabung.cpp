// Ahmad Sidik Rudini - 201106041165
// Menghitung Luas dan Volume Tabung

#include <stdio.h>
#include <conio.h>
#include <math.h>

main()
{
	float L,I,R,T;    //L=Luas permukaan tabung, I=Volume tabung, R=Jari-jari tabung, T=Tinggi tabung
	float phi=3.14;
	
	printf("Masukkan jari-jari tabung : ");
	scanf("%f",&R);
	
	printf("Masukkan tinggi tabung : ");
	scanf("%f",&T);
			
	L=2*phi*R*(R+T);		// Luas Alas + Luas Tutup + Luas Sisi Samping
	I=phi*R*R*T;			// Luas Alas * Tinggi
	
	printf("Luas permukaannya adalah : %f\n",L);
	printf("Volumenya adalah : %f\n", I);
	
	getch();
}
